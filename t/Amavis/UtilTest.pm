# SPDX-License-Identifier: GPL-2.0-or-later

package Amavis::UtilTest;

use Test::Most;
use base 'Test::Class';

sub class { 'Amavis::Util' }

sub startup : Tests(startup => 1) {
  my $test = shift;
  return 'Incomplete module';
  use_ok $test->class;
}

sub empty : Tests(0) {
}

1;
