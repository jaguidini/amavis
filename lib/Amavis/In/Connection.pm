# SPDX-License-Identifier: GPL-2.0-or-later

package Amavis::In::Connection;

# Keeps relevant information about how we received the message:
# client connection information, SMTP envelope and SMTP parameters

use strict;
use re 'taint';

BEGIN {
  require Exporter;
  use vars qw(@ISA @EXPORT @EXPORT_OK %EXPORT_TAGS $VERSION);
  $VERSION = '2.412';
  @ISA = qw(Exporter);
}

sub new
  { my $class = $_[0]; bless {}, $class }

sub client_ip      # client IP address (immediate SMTP client, i.e. our MTA)
  { @_<2 ? shift->{client_ip}   : ($_[0]->{client_ip} = $_[1]) }
sub client_port    # TCP source port number (immediate SMTP client)
  { @_<2 ? shift->{client_port} : ($_[0]->{client_port} = $_[1]) }
sub socket_ip      # IP address of our interface that received connection
  { @_<2 ? shift->{socket_ip}   : ($_[0]->{socket_ip} = $_[1]) }
sub socket_port    # TCP port of our interface that received connection
  { @_<2 ? shift->{socket_port} : ($_[0]->{socket_port} = $_[1]) }
sub socket_proto   # TCP/UNIX
  { @_<2 ? shift->{socket_proto}: ($_[0]->{socket_proto} = $_[1])}
sub socket_path    # socket path, UNIX sockets only
  { @_<2 ? shift->{socket_path} : ($_[0]->{socket_path} = $_[1])}
# RFC 3848
sub appl_proto     # SMTP/ESMTP(A|S|SA)/LMTP(A|S|SA) / AM.PDP/AM.CL/QMQP/QMQPqq
  { @_<2 ? shift->{appl_proto}  : ($_[0]->{appl_proto} = $_[1]) }
sub smtp_helo      # (E)SMTP HELO/EHLO parameter
  { @_<2 ? shift->{smtp_helo}   : ($_[0]->{smtp_helo} = $_[1]) }

1;
