#!/usr/bin/perl -T
# SPDX-License-Identifier: GPL-2.0-or-later

### profiling:
### #!/usr/bin/perl -d:NYTProf
###   NYTPROF=start=no:addpid=1:forkdepth=1 amavisd -m 5 foreground

#------------------------------------------------------------------------------
# This is amavis.
# It is an interface between a message transfer agent (MTA) and virus
# scanners and/or spam scanners, functioning as a mail content filter.
#
# It is a performance-enhanced and feature-enriched version of amavisd
# (which in turn is a daemonized version of AMaViS), initially based
# on amavisd-snapshot-20020300).
#
# All work since amavisd-snapshot-20020300:
#   Copyright (C) 2002-2018 Mark Martinec,
#   All Rights Reserved.
# with contributions from the amavis-user mailing list and individuals,
# as acknowledged in the release notes.
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

# Author: Mark Martinec <Mark.Martinec@ijs.si>
# Patches and problem reports are welcome.
#
# The latest version of this program is available at:
#   http://www.ijs.si/software/amavisd/
#------------------------------------------------------------------------------

# Here is a boilerplate from the amavisd(-snapshot) version, which is
# the version (from 2002-03) that served as a base code for the initial
# version of amavisd-new. License terms were the same:
#
#   Author:  Chris Mason <cmason@unixzone.com>
#   Current maintainer: Lars Hecking <lhecking@users.sourceforge.net>
#   Based on work by:
#         Mogens Kjaer, Carlsberg Laboratory, <mk@crc.dk>
#         Juergen Quade, Softing GmbH, <quade@softing.com>
#         Christian Bricart <shiva@aachalon.de>
#         Rainer Link <link@foo.fh-furtwangen.de>
#   This script is part of the AMaViS package.  For more information see:
#     http://amavis.org/
#   Copyright (C) 2000 - 2002 the people mentioned above
#   This software is licensed under the GNU General Public License (GPL)
#   See:  http://www.gnu.org/copyleft/gpl.html
#------------------------------------------------------------------------------

BEGIN { pop @INC if $INC[-1] eq '.' }  # CVE-2016-1238 (perl)
use sigtrap qw(stack-trace BUS SEGV EMT FPE ILL SYS TRAP);  # ABRT

use strict;
use re 'taint';
use warnings;
use warnings FATAL => qw(utf8 void);
no warnings 'uninitialized';
# use warnings 'extra'; no warnings 'experimental::re_strict'; use re 'strict';

BEGIN {
  # Will be overwritten late inside Amavis module.
  # Is used to transform a die() in top evaluation scope
  # into an exit in order to avoid compilation aborted messages:
  #  demo@vm:~ perl -T -Ilib bin/amavisd -V
  #  amavis-2.12.0 (20190725)
  #  Compilation failed in require at bin/amavisd line 96.
  #  BEGIN failed--compilation aborted at bin/amavisd line 96.
  $SIG{__DIE__} = sub {
    my $eval = 0;
    for (my $i = 0;; $i++) {
      my ($package, $subroutine) = (caller $i)[0, 3];
      last unless $package && $subroutine;
      if ($package ne 'main' && $subroutine eq '(eval)') {
          $eval = 1;
          last;
      }
    }
    if ($eval) {
      die @_;
    } else {
      print @_;
      exit 1;
    }
  }
}

use Amavis::Boot;
use Amavis;
